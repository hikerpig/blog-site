const { paginate } = require('gatsby-awesome-pagination')
const { createFilePath } = require(`gatsby-source-filesystem`)
const { forEach, forEachIndexed, uniq, filter, not, isNil, chain } = require('rambdax')
const path = require('path')
const { toKebabCase } = require('./src/helpers')
const { QIMAGE_HOST } = require('./src/config')

const pageTypeRegex = /src|content\/(.*?)\//
const getType = node => node.fileAbsolutePath.match(pageTypeRegex)[1]

const pageTemplate = path.resolve(`./src/templates/page.js`)
const indexTemplate = path.resolve(`./src/templates/index.js`)
const tagsTemplate = path.resolve(`./src/templates/tags.js`)
const archivesTemplate = path.resolve(`./src/templates/archives.js`)

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      alias: {
        src: path.resolve(__dirname, 'src'),
      },
    },
  })
}

exports.createPages = ({ actions, graphql, getNodes }) => {
  const { createPage } = actions
  const allNodes = getNodes()

  return graphql(`
    {
      allMarkdownRemark(
        sort: { fields: [frontmatter___date], order: DESC }
        limit: 1000
      ) {
        edges {
          node {
            fields {
              slug
            }
            frontmatter {
              title
              tags
            }
            fileAbsolutePath
          }
        }
      }
      site {
        siteMetadata {
          postsPerPage
        }
      }
    }
  `).then(result => {
    if (result.errors) {
      return Promise.reject(result.errors)
    }

    const {
      allMarkdownRemark: { edges: markdownPages },
      site: { siteMetadata },
    } = result.data

    const sortedPages = markdownPages.sort((pageA, pageB) => {
      const typeA = getType(pageA.node)
      const typeB = getType(pageB.node)

      return (typeA > typeB) - (typeA < typeB)
    })

    const posts = allNodes.filter(
      ({ internal, fileAbsolutePath }) =>
        internal.type === 'MarkdownRemark' &&
        fileAbsolutePath.indexOf('/posts/') !== -1,
    )

    // Create posts index with pagination
    paginate({
      createPage,
      items: posts,
      component: indexTemplate,
      itemsPerPage: siteMetadata.postsPerPage,
      pathPrefix: '/',
    })

    // Create each markdown page and post
    forEachIndexed(({ node }, index) => {
      const previous = sortedPages[index - 1] ? sortedPages[index - 1].node: null
      const next = sortedPages[index + 1] ? sortedPages[index + 1].node: null
      const isNextSameType = getType(node) === (next && getType(next))
      const isPreviousSameType =
        getType(node) === (previous && getType(previous))

      createPage({
        path: node.fields.slug,
        component: pageTemplate,
        context: {
          type: getType(node),
          next: isNextSameType ? next : null,
          previous: isPreviousSameType ? previous : null,
          slug: node.fields.slug,
        },
      })
    }, sortedPages)

    // Create archives
    createPage({
      path: '/archives',
      component: archivesTemplate,
    })

    // Create tag pages
    const tags = filter(
      tag => not(isNil(tag)),
      uniq(chain(post => post.frontmatter.tags, posts)),
    )

    forEach(tag => {
      const postsWithTag = posts.filter(
        post =>
          post.frontmatter.tags && post.frontmatter.tags.indexOf(tag) !== -1,
      )

      paginate({
        createPage,
        items: postsWithTag,
        component: tagsTemplate,
        itemsPerPage: siteMetadata.postsPerPage,
        pathPrefix: `/tag/${toKebabCase(tag)}`,
        context: {
          tag,
        },
      })
    }, tags)

    return {
      sortedPages,
      tags,
    }
  })
}

exports.sourceNodes = ({ actions }) => {
  const { createTypes } = actions
  const typeDefs = `#graphql
    type MarkdownRemark implements Node  {
      frontmatter: Frontmatter!
      fields: RemarkFields
    }

    type RemarkFields {
      slug: String
      featureImageSrc: String
    }

    type Frontmatter {
      title: String!
      author: String
      date: Date @dateformat
      category: String
      tags: [String!]
      summary: String
      coverImage: File @fileByRelativePath
      published: Boolean
      use_toc: Boolean
    }
  `
  createTypes(typeDefs)
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const slugValue = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value: slugValue,
    })

    if (node.frontmatter.feature_qimage) {
      node.frontmatter.feature_image = `${QIMAGE_HOST}/${node.frontmatter.feature_qimage}`
    }
    if (node.frontmatter.feature_image) {
      createNodeField({
        name: `featureImageSrc`,
        node,
        value: node.frontmatter.feature_image,
      })
    }
  }
}
