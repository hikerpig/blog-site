/* eslint-disable prefer-object-spread */
/* eslint-disable arrow-body-style */
const postCssPresetEnv = require(`postcss-preset-env`)
const postCSSNested = require('postcss-nested')
const postCSSUrl = require('postcss-url')
const postCSSImports = require('postcss-import')
const cssnano = require('cssnano')
const postCSSMixins = require('postcss-mixins')

const LIQUID_TAGS = require('./src/helpers/liquid-tags')

module.exports = {
  siteMetadata: {
    title: `HP goes FE`,
    description: `Hikerpig 万事记`,
    copyrights: '',
    author: `hikerpig`,
    logo: {
      src: '/logo.png',
      alt: 'Hikerpig 万事记',
    },
    logoText: 'HP goes FE',
    defaultTheme: 'dark',
    postsPerPage: 10,
    showMenuItems: 3,
    menuMoreText: '更多',
    siteUrl: process.env.SITE_URL || 'https://www.hikerpig.cn',
    mainMenu: [
      {
        title: 'About',
        path: '/about',
      },
      {
        title: '归档',
        path: '/archives',
      },
      {
        title: '📡 RSS',
        path: '/rss.xml',
      },
    ],
  },
  plugins: [
    `babel-preset-gatsby`,
    `gatsby-plugin-react-helmet`,
    'gatsby-plugin-next-seo',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/content/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `posts`,
        path: `${__dirname}/content/posts`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/src/pages`,
      },
    },
    {
      resolve: `gatsby-plugin-postcss`,
      options: {
        postCssPlugins: [
          postCSSUrl(),
          postCSSImports(),
          postCSSMixins(),
          postCSSNested(),
          postCssPresetEnv({
            importFrom: 'src/styles/variables.css',
            stage: 3,
            preserve: true,
            features: {
              'custom-media-queries': true,
              'custom-properties': false,
            },
          }),
          cssnano({
            preset: 'default',
          }),
        ],
      },
    },
    {
      resolve: `gatsby-plugin-gitalk`,
      options: {
        config: {
          clientID: '0b21579fda2f1d2e73f4',
          clientSecret: 'abc0351a314cc46bb3d58b6dad62c670c2d1e8f9',
          repo: 'hikerpig.github.io',
          owner: 'hikerpig',
          admin: ['hikerpig'],
        },
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-autolink-headers`,
            options: {
              enableCustomId: true,
            },
          },
          {
            resolve: 'gatsby-remark-mermaid',
            options: {
              language: 'mermaid',
              theme: 'default',
              viewport: {
                width: 400,
                height: 400,
              },
            },
          },
          {
            resolve: `gatsby-remark-katex`,
            options: {
              // Add any KaTeX options from https://github.com/KaTeX/KaTeX/blob/master/docs/options.md here
              strict: `ignore`,
            },
          },
          {
            resolve: 'gatsby-remark-embed-video',
            options: {
              related: false,
              noIframeBorder: true,
            },
          },
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 800,
              quality: 100,
            },
          },
          {
            resolve: `gatsby-remark-prismjs`,
            options: {
              classPrefix: 'language-',
              inlineCodeMarker: null,
              aliases: {
                zsh: 'bash',
              },
              showLineNumbers: false,
              noInlineHighlight: true,
              prompt: {
                user: 'hikerpig',
                // host: "localhost",
                global: false,
              },
            },
          },
          {
            resolve: `@hikerpig/gatsby-remark-liquid-tags`,
            options: {
              customServiceMap: {
                ...LIQUID_TAGS,
              },
            },
          },
          'gatsby-remark-numbered-footnotes',
        ],
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // The property ID; the tracking code won't be generated without it
        trackingId: 'UA-49433257-1',
        // Defines where to place the tracking script - `true` in the head and `false` in the body
        head: false,
        // Setting this parameter is optional
        anonymize: true,
        // Defers execution of google analytics script after page load
        defer: true,
      },
    },
    {
      resolve: 'gatsby-plugin-no-sourcemaps',
    },
    {
      resolve: `gatsby-plugin-feed`,
      options: {
        feeds: [
          {
            serialize: ({ query: { site, allMarkdownRemark } }) => {
              return allMarkdownRemark.edges.map((edge) => {
                return Object.assign({}, edge.node.frontmatter, {
                  description: edge.node.excerpt,
                  date: edge.node.frontmatter.date,
                  url: site.siteMetadata.siteUrl + edge.node.fields.slug,
                  guid: site.siteMetadata.siteUrl + edge.node.fields.slug,
                })
              })
            },
            query: `
              {
                allMarkdownRemark(
                  sort: { order: DESC, fields: [frontmatter___date] },
                ) {
                  edges {
                    node {
                      excerpt
                      html
                      fields { slug }
                      frontmatter {
                        title
                        date
                      }
                    }
                  }
                }
              }
            `,
            output: '/rss.xml',
            title: 'Hikerpig 万事记',
          },
        ],
      },
    },
    {
      resolve: 'gatsby-plugin-marka',
      options: {
        container: '.post__content',
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `HP goes FE`,
        start_url: `/`,
        background_color: `#292a2d`,
        theme_color: `#ff766d`,
        display: `minimal-ui`,
        icon: `content/images/favicon.png`,
      },
    },
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        output: '/',
      },
    },
    // local plugins
    'gatsby-plugin-littlefoot',
    'gatsby-plugin-tocbot',
  ],
}
