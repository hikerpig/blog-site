---
title: "关于我"
---

我是一个住在北京的前端工程师，这里是个技术博客。

间或穿插一些喜欢听的歌以及它们的入门版乐谱。

社交网络:

- <img class="inline-img" height="16" width="16" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/github.svg" /> [github](https://github.com/hikerpig)

- <img class="inline-img" height="16" width="16" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/douban.svg" /> [豆瓣](https://www.douban.com/people/hikerpig/)，半只读状态。

- <img class="inline-img" height="16" width="16" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/twitter.svg" /> [twitter](https://twitter.com/Hikerpig)，好久没说话。