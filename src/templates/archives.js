import React from 'react'
import PropTypes from 'prop-types'
import { graphql, Link } from 'gatsby'
import { groupBy, reverse } from 'rambdax'
import SEO from '../components/seo'
import Layout from '../components/layout'

const ArchivePosts = ({ posts }) => {
  const sectionGroups = groupBy(post => {
    const d = new Date(post.frontmatter.date)
    return d.getFullYear()
  }, posts)

  const sectionEles = Object.keys(sectionGroups).map(key => {
    const yearPosts = sectionGroups[key]
    return (
      <section className="archive__section">
        <header className="archive__section-header">
          <h3>{key}</h3>
        </header>
        <div>
          {yearPosts.map(post => {
            const {
              frontmatter: { date, title },
              fields: { slug },
            } = post
            return (
              <div className="archive__item">
                <Link to={slug}>
                  <span className="archive__item-date">{date}</span>
                  <div className="archive__item-title">{title}</div>
                </Link>
              </div>
            )
          })}
        </div>
      </section>
    )
  })

  return <div className="archive__posts">{reverse(sectionEles)}</div>
}

const Index = ({ data }) => {
  const {
    allMarkdownRemark: { edges: posts },
  } = data

  return (
    <>
      <SEO />
      <Layout>
        <ArchivePosts posts={posts.map(o => o.node)}></ArchivePosts>
      </Layout>
    </>
  )
}

Index.propTypes = {
  data: PropTypes.object.isRequired,
  pageContext: PropTypes.shape({
    nextPagePath: PropTypes.string,
    previousPagePath: PropTypes.string,
  }),
}

export const archivesQuery = graphql`
  query {
    allMarkdownRemark(
      filter: {
        fileAbsolutePath: { regex: "//posts//" }
        frontmatter: { published: { ne: false } }
      }
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      edges {
        node {
          id
          fields {
            slug
          }
          frontmatter {
            title
            date(formatString: "YYYY-MM-DD")
          }
        }
      }
    }
  }
`

export default Index
