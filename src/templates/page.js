import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import { BlogPostJsonLd } from 'gatsby-plugin-next-seo'

import '@suziwen/gitalk/dist/gitalk.css'

import Loadable from '@loadable/component'
import SEO from '../components/seo'
import Layout from '../components/layout'
import Post from '../components/post'


const GitalkComponent = Loadable(() => import('@suziwen/gitalk/dist/gitalk-component'))

const BlogPostTemplate = ({ data, pageContext }) => {
  const {
    frontmatter: { title, date, coverImage, summary, tags },
    excerpt: autoExcerpt,
    fields,
    id,
    html,
  } = data.markdownRemark
  const { next, previous } = pageContext
  const { frontmatter } = data.markdownRemark
  const { featureImageSrc } = fields
  const author = frontmatter.author || data.site.siteMetadata.author
  const { siteUrl } = data.site.siteMetadata
  // console.log('front matter', frontmatter)
  const gitalkConfig = typeof GATSBY_GITALK_CONFIG !== 'undefined' ? GATSBY_GITALK_CONFIG : null
  const description = summary || autoExcerpt

  const pageUrl = `${siteUrl}${fields.slug}`
  const publisherLogo = `${siteUrl}/logo.png`

  return (
    <Layout>
      <SEO title={title} description={description} />
      <BlogPostJsonLd
        url={pageUrl}
        title={title}
        images={featureImageSrc ? [featureImageSrc] : []}
        authorName={author}
        description={description}
        overrides={{
          publisher: {
            '@type': 'Person',
            name: author,
            logo: {
              '@type': 'ImageObject',
              url: publisherLogo,
            },
          },
        }}
      />

      {frontmatter.use_toc ? <div id="toc" className="js-toc toc tocbot" /> : null}
      <Post
        key={id}
        title={title}
        date={date}
        path={fields.slug}
        author={author}
        coverImage={coverImage}
        html={html}
        tags={tags}
        previousPost={previous}
        nextPost={next}
        excerpt={autoExcerpt}
        isDetail
      />
      {gitalkConfig ? (
        <GitalkComponent
          options={{
            ...gitalkConfig,
            id: fields.slug,
            title,
            labels: ['Gitalk'],
          }}
        />
      ) : null}
    </Layout>
  )
}

export default BlogPostTemplate

BlogPostTemplate.propTypes = {
  data: PropTypes.object.isRequired,
  pageContext: PropTypes.shape({
    next: PropTypes.object,
    previous: PropTypes.object,
  }),
}

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        author
        siteUrl
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      html
      excerpt
      fields {
        slug
        featureImageSrc
      }
      frontmatter {
        title
        date(formatString: "YYYY-MM-DD")
        author
        tags
        use_toc
        coverImage {
          childImageSharp {
            fluid(maxWidth: 800) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`
