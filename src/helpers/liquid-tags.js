const { QIMAGE_HOST } = require('../config')

function qimage(first) {
  var [p, title] = first
  p = p.replace(/^/, '')
  const src = `${QIMAGE_HOST}/${p}`
  return `<img src="${src}" alt="${title}" title="${title}">`
}

function qfigure(first) {
  var [p, title] = first
  return `
  <figure>
    ${qimage(first)}
    <figcaption>${title}</figcaption>
  </figure>
  `
}

module.exports = {
  qimage,
  qfigure,
}
