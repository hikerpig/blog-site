import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import Img from 'gatsby-image'
import Navigation from './navigation'
import { toKebabCase } from '../helpers'

import * as style from '../styles/post.module.css'

const Post = ({
  title,
  date,
  path,
  coverImage,
  featureImageSrc,
  author,
  excerpt,
  tags,
  html,
  previousPost,
  nextPost,
  isDetail,
}) => {
  const previousPath = previousPost && previousPost.fields.slug
  const previousLabel = previousPost && previousPost.frontmatter.title
  const nextPath = nextPost && nextPost.fields.slug
  const nextLabel = nextPost && nextPost.frontmatter.title

  return (
    <article className={`${style.post} post h-entry`}>
      <div className={style.postContent}>
        <h1 className={`${style.title} p-name`}>
          {!isDetail ? <Link to={path} className="post__title">{title}</Link> : title}
        </h1>
        <div className={style.meta}>
          <div>
            <time className="dt-published" time={date}>{date}</time>
            {author && (<> - <span className="p-author">{author}</span></>) }
          </div>
          {tags ? (
            <div className={style.tags}>
              {tags.map(tag => (
                <Link to={`/tag/${toKebabCase(tag)}/`} key={toKebabCase(tag)}>
                  <span className={style.tag}>#{tag}</span>
                </Link>
              ))}
            </div>
          ) : null}
        </div>

        {coverImage && (
          <Img
            fluid={coverImage.childImageSharp.fluid}
            className={style.coverImage}
          />
        )}

        {featureImageSrc && (
          <img src={featureImageSrc} className={style.coverImage} />
        )}

        {!isDetail ? (
          <div className="p-summary">
            <p>{excerpt}</p>
            <Link to={path} className={style.readMore}>
              Read more →
            </Link>
          </div>
        ) : (
          <>
            <div className="post__content e-content" dangerouslySetInnerHTML={{ __html: html }} />
            <Navigation
              previousPath={previousPath}
              previousLabel={previousLabel}
              nextPath={nextPath}
              nextLabel={nextLabel}
            />
          </>
        )}
      </div>
    </article>
  )
}

Post.propTypes = {
  title: PropTypes.string,
  date: PropTypes.string,
  path: PropTypes.string,
  coverImage: PropTypes.object,
  featureImageSrc: PropTypes.string,
  author: PropTypes.string,
  excerpt: PropTypes.string,
  html: PropTypes.string,
  tags: PropTypes.arrayOf(PropTypes.string),
  previousPost: PropTypes.object,
  nextPost: PropTypes.object,
  isDetail: PropTypes.bool,
}

export default Post
