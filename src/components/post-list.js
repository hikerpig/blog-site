import React from 'react'
import PropTypes from 'prop-types'
import Post from '../components/post'

export default function PostList({ posts }) {
  return (
    <div className="post-list">
      {posts.map(({ node }) => {
        const {
          id,
          excerpt: autoExcerpt,
          fields,
          frontmatter: {
            title,
            date,
            author,
            coverImage,
            tags,
            summary,
          },
        } = node

        return (
          <Post
            key={id}
            title={title}
            date={date}
            path={fields.slug}
            author={author}
            coverImage={coverImage}
            featureImageSrc={fields.featureImageSrc}
            tags={tags}
            excerpt={ summary || autoExcerpt}
          />
        )
      })}
    </div>
  )
}

PostList.propTypes = {
  posts: PropTypes.object,
}