// prismjs color theme
require('./src/styles/syntax.css')
require('./src/styles/toc.css')
require('./src/styles/post.css')
require('./src/styles/archive.css')
require('./src/styles/vendors.css')

require('prismjs/plugins/command-line/prism-command-line.css')
