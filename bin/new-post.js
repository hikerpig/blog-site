const moment = require('moment')
const fs = require('fs')
const yargs = require('yargs')

const { argv } = yargs

function getCurrentDateStr() {
  const d = new moment()
  const dateStr = d.format('YYYY-MM-DD')
  return dateStr
}

function genFileContent({ title }) {
  const content = `---
layout: post
date: ${getCurrentDateStr()}
title: ${title}
description: ""
summary: ""
published: True
category: ""
tags:
---

`
  return content
}

function main() {
  const title = argv.title || ''
  const content = genFileContent({ title })
  const fileName = `_posts/${getCurrentDateStr()}-${title}.md`
  console.log('write to file:', fileName)
  fs.writeFileSync(fileName, content)
}

main()