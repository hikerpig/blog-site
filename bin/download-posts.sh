#/usr/bin/env bash
tar_name='posts.tar'

wget https://gitlab.com/hikerpig/blog-posts/-/archive/master/blog-posts-master.tar -O $tar_name
tar xf $tar_name

mkdir -p content/posts

cp blog-posts-master/* content/posts/

