/* eslint-disable react/no-danger */
import React from 'react'

/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */

export const onPreRenderHTML = ({
  getPostBodyComponents,
  replacePostBodyComponents,
  getPreBodyComponents,
  replacePreBodyComponents,
}) => {
  const asyncStyles = [
    ['https://fonts.googleapis.com/css2?family=Noto+Sans+SC:wght@400;700&display=swap', { media: 'min-width: 1024px' }],
    'https://cdn.jsdelivr.net/npm/katex@0.13.18/dist/katex.min.css',
    'https://cdn.jsdelivr.net/npm/prism-themes@1.8.0/themes/prism-hopscotch.css',
  ]

  const preBodyComponents = getPreBodyComponents()
  // async stylesheet mocking, @link https://discourse.wicg.io/t/a-standard-method-for-loading-style-sheets-asynchronously/542/7
  const texts = asyncStyles
    .map(item => {
      const isArray = Array.isArray(item)
      const href = isArray ? item[0] : item
      const opts = isArray ? item[1] : {}
      return `<link href="${href}" rel="preload" as="style" onload="requestAnimationFrame(() => this.rel='stylesheet')" ${
        opts.media ? `media="${opts.media}"` : ''
      }></link>`
    })
    .join('\n')
  preBodyComponents.push(<div dangerouslySetInnerHTML={{ __html: texts }}></div>)
  replacePreBodyComponents(preBodyComponents)

  // const postBodyComponents = getPostBodyComponents()
  // postBodyComponents.push(
  //   <>
  //     <div id="search">
  //       <input type="search" />
  //     </div>
  //     <link
  //       rel="stylesheet"
  //       href="https://cdn.jsdelivr.net/npm/@algolia/algoliasearch-netlify-frontend@0/dist/algoliasearchNetlify.css"
  //     />
  //     <script
  //       type="text/javascript"
  //       src="https://cdn.jsdelivr.net/npm/@algolia/algoliasearch-netlify-frontend@0/dist/algoliasearchNetlify.js"
  //     />
  //     <script
  //       type="text/javascript"
  //       dangerouslySetInnerHTML={{
  //         __html: `
  //     algoliasearchNetlify({
  //       appId: 'RG8LB8NU9C',
  //       apiKey: '344a58aa4256827e0fa319f7464307d7',
  //       siteId: '3c328b6d-dbb8-4b8a-8258-7bc5e8fa7c88',
  //       selector: 'div#search',
  //       branch: 'master',
  //       autocomplete: {
  //         inputSelector: 'input[type=search]',
  //       },
  //     });
  //   `,
  //       }}
  //     />
  //   </>,
  // )

  // replacePostBodyComponents(postBodyComponents)
}
