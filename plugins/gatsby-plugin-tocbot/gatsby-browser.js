/* global tocbot:false */
function callTocbot() {
  tocbot.init({
    contentSelector: '.post',
    collapseDepth: 5,
  })
}

export const onInitialClientRender = function() {
  const styleLink = document.createElement('link')
  styleLink.rel = 'stylesheet'
  styleLink.href = 'https://cdn.jsdelivr.net/npm/tocbot/dist/tocbot.css'
  document.head.appendChild(styleLink)

  const script = document.createElement('script')
  script.type = 'application/javascript'
  script.src = 'https://cdn.jsdelivr.net/npm/tocbot/dist/tocbot.js'
  document.body.appendChild(script)

  script.addEventListener('load', () => {
    callTocbot()
  })
}

export const onRouteUpdate = () => {
  if (window.tocbot) {
    callTocbot()
  } else {
    window.addEventListener('DOMContentLoaded', () => {
      callTocbot()
    })
  }
}