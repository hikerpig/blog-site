/* global littlefoot:false */
// ---------------- littlefoot ----------------------
function callLittlefoot() {
  littlefoot.littlefoot({
    allowMultiple: true,
    buttonTemplate: `
  <button
    aria-expanded="false"
    aria-label="Footnote <% number %>"
    class="littlefoot-footnote__button"
    id="<% reference %>"
    title="See Footnote <% number %>"
  />
    <% number %>
  </button>
  `,
  })
}

export const onInitialClientRender = function() {
  const styleLink = document.createElement('link')
  styleLink.rel = 'stylesheet'
  styleLink.href = 'https://unpkg.com/littlefoot/dist/littlefoot.css'
  document.head.appendChild(styleLink)

  const script = document.createElement('script')
  script.type = 'application/javascript'
  script.src = 'https://unpkg.com/littlefoot/dist/littlefoot.js'
  document.body.appendChild(script)

  script.addEventListener('load', () => {
    callLittlefoot()
  })
}

export const onRouteUpdate = () => {
  if (window.littlefoot) {
    callLittlefoot()
  } else {
    window.addEventListener('DOMContentLoaded', () => {
      callLittlefoot()
    })
  }
}
// ----------------end littlefoot -------------------
